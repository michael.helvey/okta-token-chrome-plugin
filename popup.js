const button = document.getElementById("getToken")

button.addEventListener("click", async () => {
  let [tab] = await chrome.tabs.query({ active: true, currentWindow: true });

  chrome.scripting.executeScript({
    target: { tabId: tab.id },
    function: getToken,
  });

  button.innerText = "Copied!"
  window.close()
})

function getToken() {
  const json = JSON.parse(localStorage.getItem("okta-token-storage"))
  const token = json["accessToken"]["accessToken"]

  const copyDiv = document.createElement('div');
  copyDiv.contentEditable = true;
  document.body.appendChild(copyDiv);

  copyDiv.innerHTML = token;
  copyDiv.unselectable = "off";
  copyDiv.focus();

  document.execCommand('SelectAll');
  document.execCommand("Copy", false, null);
  document.body.removeChild(copyDiv);
}
