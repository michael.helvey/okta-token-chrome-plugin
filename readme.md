# Okta Token Grabber

Not sure what to call this.  I got slightly annoyed by having to parse and
copy/paste JSON out of localStorage when running UIs locally so I wrote a chrome
plugin to do it for me.  Yay for bikeshedding, amirite.

## Installation

* Turn on developer mode in Chrome and load unpacked extension
* Click on the plugin when you have the vendor portal open (and signed in)
* Click the big button that says "get token" to copy the Okta token to your
  clipboard
* Profit
